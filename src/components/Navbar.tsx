import { Flex, Link } from '@chakra-ui/react';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

export const Navbar = () => {
  return (
    <Flex gap={5} alignItems="center" justifyContent="center">
      <Link as={RouterLink} to="/">
        Home
      </Link>
      <Link as={RouterLink} to="/search">
        Search Data
      </Link>
      {/* <Link as={RouterLink} to="/submit">
        Submit Data
      </Link> */}
      <Link as={RouterLink} to="/about">
        About
      </Link>
    </Flex>
  );
};
