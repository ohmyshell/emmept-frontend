import { Flex, Input, VStack } from '@chakra-ui/react';
import fuzzysort from 'fuzzysort';
import React, { useCallback, useEffect, useState } from 'react';
import { File } from 'src/models/File';
import { ResultList } from './ResultList';

interface AdvancedSearchProps {
  history: File[];
}

export const AdvancedSearch = ({ history }: AdvancedSearchProps) => {
  const [searchValue, setSearchValue] = useState('');
  const [results, setResults] = useState<any[]>([]);

  const filterResultsRec = useCallback(
    (res: File[], parentName: string) => {
      if (!res) return;

      setResults((prevResults) => [
        ...prevResults,
        ...fuzzysort
          .go(searchValue, res, { key: 'name', limit: 10, threshold: -100 })
          .map((elm) => ({ ...elm.obj, parent: parentName })),
      ]);

      res.forEach((historyElement) =>
        filterResultsRec(historyElement.children, historyElement.name)
      );
    },
    [setResults, searchValue]
  );

  const filterResults = useCallback(
    (history: File[]) => {
      setResults([]);
      if (searchValue !== '') filterResultsRec(history, '/');
    },
    [filterResultsRec, searchValue]
  );

  useEffect(() => {
    filterResults(history);
  }, [searchValue, filterResults, history]);

  return (
    <VStack>
      <Input
        placeholder="Search everything"
        boxShadow="md"
        textAlign="center"
        value={searchValue}
        onChange={(event) => {
          setSearchValue(event.target.value);
        }}
        _placeholder={{ opacity: 1, color: 'green.500' }}
      />
      <Flex wrap="wrap" gap="5" justifyContent="space-between">
        <ResultList results={results} />
      </Flex>
    </VStack>
  );
};
