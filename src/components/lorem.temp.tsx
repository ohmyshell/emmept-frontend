import React from 'react';

interface LoremProps {
  count: number;
}

export const Lorem = ({ count }: LoremProps) => {
  const loremText =
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit consequatur ipsum dolores, placeat doloribus molestias optio unde quia nulla est inventore dolorum quidem sed earum veniam iusto itaque eos mollitia!';

  return (
    <>
      {Array(count)
        .fill(loremText)
        .map((lorem) => lorem)}
    </>
  );
};
