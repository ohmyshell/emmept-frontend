import { Box, Grid, Image } from '@chakra-ui/react';
import React from 'react';
import { ColorModeSwitcher } from './ColorModeSwitcher';
import { Navbar } from './Navbar';

export const Header = () => {
  return (
    <Grid gridTemplateColumns="repeat(3, 1fr)" p={4}>
      <Box>
        <Image
          src="https://images.vexels.com/media/users/3/156051/isolated/preview/72094c4492bc9c334266dc3049c15252-flat-flower-icon-flower-by-vexels.png"
          _dark={{ filter: 'invert(100%)' }}
          boxSize="100px"
        />
        MICROPLANT
      </Box>
      <Navbar />
      <ColorModeSwitcher justifySelf="flex-end" alignSelf="center" />
    </Grid>
  );
};
