import { Image, Skeleton, Stack, Text } from '@chakra-ui/react';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';

interface ImageDisplayProps {
  onClick: () => void;
  text: string;
  imageSrc: string;
}

const ImageDisplay = ({ onClick, text, imageSrc }: ImageDisplayProps) => (
  <Stack key={uuidv4()}>
    <Text>{text}</Text>
    <Image
      onClick={onClick}
      fallback={<Skeleton minHeight="250px" />}
      cursor="pointer"
      width="100%"
      height="auto"
      src={imageSrc}
    />
  </Stack>
);

export default ImageDisplay;
