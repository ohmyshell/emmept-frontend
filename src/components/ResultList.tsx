import { ExternalLinkIcon } from '@chakra-ui/icons';
import {
  Box,
  Button,
  Link,
  List,
  ListItem,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import { File } from 'src/models/File';
import { v4 as uuidv4 } from 'uuid';

interface ResultListProps {
  results: File[];
}

export const ResultList = ({ results }: ResultListProps) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [selectedResult, setSelectedResult] = useState<File>();

  return (
    <>
      {results.map((result) => (
        <Button
          key={uuidv4()}
          variant="ghost"
          onClick={() => {
            setSelectedResult(result);
            onOpen();
          }}
        >
          {result.name}
        </Button>
      ))}

      <Modal
        isOpen={isOpen}
        onClose={onClose}
        isCentered
        scrollBehavior="inside"
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Plant Description</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {selectedResult?.id && (
              <Link
                href={`https://drive.google.com/uc?id=${selectedResult?.id}`}
                isExternal
                fontWeight="bold"
              >
                Image Link <ExternalLinkIcon mx="2px" />
              </Link>
            )}
            <Box>
              <Text fontWeight="bold">Name: </Text>
              <Text>{selectedResult?.name}</Text>
            </Box>

            {selectedResult?.parent && (
              <Box>
                <Text fontWeight="bold">Parent: </Text>
                <Text>{selectedResult?.parent}</Text>
              </Box>
            )}

            {selectedResult?.children && (
              <Box>
                <Text fontWeight="bold">Children:</Text>
                <List>
                  {selectedResult?.children?.map((child) => (
                    <ListItem key={uuidv4()}>{child.name}</ListItem>
                  ))}
                </List>
              </Box>
            )}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};
