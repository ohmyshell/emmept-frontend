import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons';
import {
  IconButton,
  Image,
  Modal,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';
import React from 'react';
import { File } from 'src/models/File';
import { v4 as uuidv4 } from 'uuid';

interface ImageOverlayProps {
  isOpen: boolean;
  onClose: () => void;
  setCurrentImage: (currentImage: number) => void;
  currentImage: number;
  imagesLength: number;
  history: File[];
}

const ImageOverlay = ({
  isOpen,
  onClose,
  setCurrentImage,
  currentImage,
  imagesLength,
  history,
}: ImageOverlayProps) => (
  <Modal isOpen={isOpen} onClose={onClose} size="full">
    <ModalOverlay>
      <ModalContent>
        <ModalCloseButton
          border="2px solid black"
          color="black"
          onClick={() => {
            setCurrentImage(0);
            onClose();
          }}
        />
        <Text
          fontSize="2xl"
          backgroundColor="blackAlpha.700"
          color="white"
          position="absolute"
          padding="2"
          left="2"
          top="2"
          rounded="base"
        >
          {
            history[history.length - 1]?.children[currentImage].name.split(
              '.'
            )[0]
          }
        </Text>
        <IconButton
          aria-label="right-arrow"
          position="absolute"
          border="2px solid black"
          color="black"
          left="2"
          top="50%"
          onClick={() => {
            const imageIndex = Math.min(
              Math.max(0, currentImage - 1),
              imagesLength - 1
            );
            setCurrentImage(imageIndex);
          }}
          zIndex={2}
        >
          <ArrowLeftIcon />
        </IconButton>

        <IconButton
          aria-label="left-arrow"
          position="absolute"
          border="2px solid black"
          color="black"
          right="2"
          top="50%"
          onClick={() => {
            const imageIndex = Math.min(
              Math.max(0, currentImage + 1),
              imagesLength - 1
            );
            setCurrentImage(imageIndex);
          }}
          zIndex={2}
        >
          <ArrowRightIcon />
        </IconButton>

        <Image
          onClick={() => {}}
          key={uuidv4()}
          width="100%"
          height="auto"
          src={`https://drive.google.com/uc?id=${
            history[history.length - 1]?.children[currentImage].id
          }`}
        />
      </ModalContent>
    </ModalOverlay>
  </Modal>
);

export default ImageOverlay;
