import { SunIcon } from '@chakra-ui/icons';
import { Button, Stack } from '@chakra-ui/react';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';

interface FolderProps {
  onClick: () => void;
  text: string;
}

const Folder = ({ onClick, text }: FolderProps) => (
  <Stack direction="row" key={uuidv4()} alignItems="center">
    <SunIcon color="green.500" />
    <Button onClick={onClick}>{text}</Button>
  </Stack>
);

export default Folder;
