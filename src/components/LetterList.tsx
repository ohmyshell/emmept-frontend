import { ArrowBackIcon } from '@chakra-ui/icons';
import { Box, Button, Grid, useDisclosure } from '@chakra-ui/react';
import React, { useState } from 'react';
import { File } from 'src/models/File';
import { v4 as uuidv4 } from 'uuid';
import Folder from './Folder';
import ImageDisplay from './ImageDisplay';
import ImageOverlay from './ImageOverlay';

const isFolder = (file: File[]) => {
  if (file) return !file[0]?.name?.includes('.jp');
  else return false;
};

interface LetterListProps {
  history: File[];
  setHistory: (file: File[]) => void;
}

export const LetterList = ({ history, setHistory }: LetterListProps) => {
  const [currentImage, setCurrentImage] = useState(0);
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Box mb="4">
        {history.length > 1 ? (
          <Button
            onClick={() => setHistory(history.slice(0, history.length - 1))}
          >
            <ArrowBackIcon />
          </Button>
        ) : null}
      </Box>

      <Grid templateColumns="repeat(4, 1fr)" gap={6}>
        {isFolder(history[history.length - 1]?.children)
          ? history[history.length - 1]?.children
              .sort((first, second) => (first.name > second.name ? 1 : -1))
              .map((file) => (
                <Folder
                  key={uuidv4()}
                  onClick={() => {
                    setHistory([...history, file]);
                  }}
                  text={file.name}
                />
              ))
          : history[history.length - 1]?.children.map((image, i) => (
              <ImageDisplay
                key={uuidv4()}
                onClick={() => {
                  setCurrentImage(i);
                  onOpen();
                }}
                text={image.name.split('.')[0]}
                imageSrc={`https://drive.google.com/uc?id=${image.id}`}
              />
            ))}

        <ImageOverlay
          isOpen={isOpen}
          onClose={onClose}
          currentImage={currentImage}
          history={history}
          imagesLength={history[history.length - 1]?.children.length}
          setCurrentImage={setCurrentImage}
        />
      </Grid>
    </>
  );
};
