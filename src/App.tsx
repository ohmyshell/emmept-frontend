import { ChakraProvider, theme } from '@chakra-ui/react';
import React from 'react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { Header } from './components/Header';
import { About } from './views/About';
import { Home } from './views/Home';
import { NotFound } from './views/NotFound';
import { Search } from './views/Search';

export const App = () => (
  <ChakraProvider theme={theme}>
    <MemoryRouter>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="search" element={<Search />} />
        {/* <Route path="submit" element={<Submit />} /> */}
        <Route path="about" element={<About />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </MemoryRouter>
  </ChakraProvider>
);
