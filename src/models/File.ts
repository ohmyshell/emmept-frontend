export type File = {
  name: string;
  children: File[];
  id?: string;
  parent?: string;
};
