import {
  Container,
  Heading,
  Link,
  ListItem,
  UnorderedList,
} from '@chakra-ui/react';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

export const About = () => {
  return (
    <Container maxW="container.lg" p={4}>
      <Heading>How to use Microplant</Heading>

      <UnorderedList>
        <ListItem>
          Navigate to{' '}
          <Link as={RouterLink} to="/search" color="teal">
            Search Data.
          </Link>{' '}
          You will be presented with two tabs.
        </ListItem>
        <ListItem>
          The first tab, family search, orders the plants according to family{' '}
          {'=>'} species.
        </ListItem>

        <ListItem>
          The second tab, advanced search, allows you to search the whole
          hierarchy and provides information based on your selection (with image
          links).
        </ListItem>
      </UnorderedList>
    </Container>
  );
};
