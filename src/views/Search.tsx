import { Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { AdvancedSearch } from 'src/components/AdvancedSearch';
import { LetterList } from 'src/components/LetterList';
import { File } from 'src/models/File';

export const Search = () => {
  const [history, setHistory] = useState<File[]>([]);

  useEffect(() => {
    import('../files.json').then((files) => {
      setHistory([files.default as File]);
    });
  }, []);

  return (
    <Tabs isFitted>
      <TabList>
        <Tab>Family Search</Tab>
        <Tab>Advanced Search</Tab>
      </TabList>
      <TabPanels>
        <TabPanel>
          <LetterList history={history} setHistory={setHistory} />
        </TabPanel>
        <TabPanel>
          <AdvancedSearch history={history} />
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
};
