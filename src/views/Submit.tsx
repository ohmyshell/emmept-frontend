import { Container, Heading, Text } from '@chakra-ui/react';
import React from 'react';

export const Submit = () => {
  return (
    <Container maxW="container.lg" p={4}>
      <Heading>Contribute</Heading>
      <Text>
        In order to contibute to this database, please send an email to:
      </Text>
    </Container>
  );
};
