import { Container, Heading, HStack, Image, Text } from '@chakra-ui/react';
import React from 'react';
import EU from '../assets/EU logo.jpg';
import N3 from '../assets/logo N3.jpg';
import PIM from '../assets/PiM logo.jpg';
import RIF from '../assets/RIF logo.jpg';
import SF from '../assets/SF-LOGO.jpg';

export const Home = () => {
  return (
    <Container maxW="container.lg" p={4}>
      <Heading>
        MicroPlant: Open access database of plant microremains for Cypriot flora
      </Heading>
      <Text>
        MicroPlant is a database of plant microremains for plants that
        characterize modern-day Cypriot flora. It contains primarily starches,
        but also phytoliths, pollen and fibers. When available information on
        the exact location and period of plant location is also provided as this
        may affect the type of microremains produced by each plant. The database
        is organized per family; within each family folder, different species
        can be found. The MicroPlant database currently contains 125 species and
        will be continuously updated. Users can search the database using the
        Search Data tab, where all families are organized alphabetically. We
        would be grateful if you could contact us in order to bring to our
        attention corrections as well as to contribute additional data. The
        latter can also be done via the Submit Data tab. Development of the
        MicroPlant database was supported by the European Regional Development
        Fund and the Republic of Cyprus through the Research and Innovation
        Foundation [grant number: EXCELLENCE/1216/0023], as well as the Wellcome
        Trust.
      </Text>
      <HStack alignItems="center" justifyContent="center" spacing={16}>
        <Image boxSize="50%" src={EU} />
        <Image boxSize="50%" src={N3} />
        <Image boxSize="50%" src={PIM} />
        <Image boxSize="50%" src={RIF} />
        <Image boxSize="50%" src={SF} />
      </HStack>
    </Container>
  );
};
