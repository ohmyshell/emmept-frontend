import { Container, Heading, Text } from '@chakra-ui/react';
import React from 'react';

export const NotFound = () => {
  return (
    <Container maxW="container.lg" p={4}>
      <Heading>404 Not Found</Heading>
      <Text>The following page was not found!</Text>
    </Container>
  );
};
